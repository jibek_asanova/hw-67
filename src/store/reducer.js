const initialState = {
    password: '',
    mainPassword: '1111',
    correctPassword: null,
};

const reducer = (state = initialState, action) => {
    if(action.type === 'PASSWORD') {
        if(state.password.length === 4) {
            alert('НЕ БОЛЬШЕ 4 СИМВОЛОВ');
            return state;
        } else {
            return {...state, password: state.password + action.payload}
        }
    }

    if(action.type === 'DELETE') {
        return {...state, password: state.password.substring(0, state.password.length - 1), correctPassword: null}
    }

    if(action.type === 'CORRECT_PASSWORD') {
        if(state.password === state.mainPassword) {
            return {...state, correctPassword: true}
        } else {
            return {...state, correctPassword: false}
        }
    }
    return state;
};

export default reducer;