import React from 'react';
import './Password.css';
import {useDispatch, useSelector} from "react-redux";

const Password = () => {
    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
    const dispatch = useDispatch();
    const password = useSelector(state => state.password);
    const correctPassword = useSelector(state => state.correctPassword);


    let stars = '';
    for(let i = 0; i < password.length; i++) {
        stars += '*';
    }



    const click = (number) => dispatch({
        type: 'PASSWORD', payload: number
    });

    const deleteNumber = () => dispatch({
        type: 'DELETE'
    });

    const enterPassword = () => dispatch({
        type: 'CORRECT_PASSWORD',
    });

    const onAreaChange = (e) => dispatch({
        type: 'PASSWORD', payload: e.target.value
    });


    return (
        <div className="PasswordBlock">
            <textarea
                className={correctPassword === null ? "PasswordTextArea" :
                    correctPassword === false ? "PasswordTextArea Wrong" : "PasswordTextArea Correct"
                }
                value={correctPassword === null ? stars : correctPassword === true ? 'Access Granted' : 'Access Denied'}
                onChange={onAreaChange}
            >
            </textarea>

            <div className="PasswordButtons">
                {numbers.map(number => (
                    <button className="Btn" key={number} onClick={() => click(number)}>{number}</button>
                ))}
                <button className="Btn" onClick={deleteNumber}>DEL</button>
                <button className="Btn" onClick={enterPassword}>E</button>
            </div>
        </div>

    );
};

export default Password;